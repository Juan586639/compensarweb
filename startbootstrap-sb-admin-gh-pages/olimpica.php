<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Compensar</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Luckiest+Guy&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Luckiest+Guy&family=Montserrat:wght@100;200&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
        <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
        <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="top">
            <div class="image-top">
              <a href="index.html"><img src="images/logo2.png" alt=""></a>
            </div>        
            <div class="buttons">
              <div class="dropdown">
                <a type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <h4>Tabla de posiciones</h4>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="cencosud.php">Jumbo o Metro</a>
                  <a class="dropdown-item" href="olimpica.php">Olímpica</a>
                </div>
              </div>
              <a class="terminos" href="index.html"><h4>Página principal</h4></a>
                <a class="terminos" href="terminos.html"><h4>Términos y Condiciones</h4></a>
            </div>           
          </nav>
        <div>
            <img class="fondo" src="images/top.png" alt="">
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <div class="card mb-4">
                            <div class="card-header">
                                <i></i>
                                <h8 style="color: white;">Ranking de los afiliados que compraron en Olímpica</h8>
                            </div>
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>Puesto</th>
                                            <th>Nickname</th>
                                            <th>Puntaje</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Puesto</th>
                                            <th>Nickname</th>
                                            <th>Puntaje</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                             $servidor = 'temporadaescolarcompensar.com';
                                             $usuario =  'temporad';
                                             $pass = '69G5HgcaYjM!ze-';
                                             $baseDatos = 'temporad_compensar-runner-cup';
                                             $counter =0;
                                             $conexion = new mysqli($servidor, $usuario, $pass, $baseDatos);
                                            $query = "SELECT * FROM players WHERE mall = 'olimpica' ORDER BY score DESC";
                                            $result_tasks = mysqli_query($conexion, $query);  
                                        while($row2 = mysqli_fetch_assoc($result_tasks)) { 
                                            $counter +=1;
                                            $tempplayername1 =$row2['nickname'];
                                            $tempplayerscore =$row2['score'];
                                        ?>
                                        <tr>
                                        <td><?php echo $counter; ?></td>
                                        <td><?php echo $tempplayername1; ?></td>
                                        <td><?php echo $tempplayerscore; ?></td>
                                        </tr>
                                     <?php } ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                          
                    </div>
                </main>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
    </body>
</html>

<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script>
    
    function scrollToTop(idname) {
        var elmnt = document.getElementById(idname);
      elmnt.scrollIntoView(true); // Top
    }
    
</script>

